class Card {
    constructor(post, user) {
        this.post = post;
        this.user = user;
    }
  
    render() {
    
        const title = document.createElement('h2');
        title.textContent = this.post.title;
        
        const card = document.createElement('div');
        card.classList.add('card');
  
        const text = document.createElement('p');
        text.textContent = this.post.body;
  
        const userInfo = document.createElement('div');
        userInfo.textContent = `${this.user.name} ${this.user.username} (${this.user.email})`;
  
        const deleteButton = document.createElement('button');
        deleteButton.classList.add('delete');
        deleteButton.dataset.postid = this.post.id;
        deleteButton.textContent = 'Delete';
        deleteButton.addEventListener('click', () => this.deleteCard());
  
        card.appendChild(title);
        card.appendChild(text);
        card.appendChild(deleteButton);
  
        return card;
    }
  
    deleteCard() {
      document.addEventListener('click', event => {
        event.preventDefault();
        if (event.target.classList.contains('delete')) {
          const postId = +event.target.getAttribute('data-postid');
          if(postId === this.post.id){
          fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, { method: 'DELETE' })
            .then(() => {
              event.target.parentElement.remove();
            });
          }
        } });
    } }
  
  document.addEventListener('DOMContentLoaded', () => {
    const mainContent = document.getElementById('mainContent');
    const loadingAnimation = document.getElementById('loadingAnimation');
    const addPostBtn = document.getElementById('addPostBtn');
    const closeBtn = document.getElementsByClassName('close')[0];
    const savePostBtn = document.getElementById('savePostBtn');
    const postTitleInput = document.getElementById('postTitle');
    const postContentInput = document.getElementById('postContent');
  
    addPostBtn.addEventListener('click', () => {
        postTitleInput.value = '';
        postContentInput.value = '';
        postModal.style.display = 'block';
    });
  
    savePostBtn.addEventListener('click', () => {
        const newPost = {
            title: postTitleInput.value,
            body: postContentInput.value,
            userId: 1,
            id: Date.now(),
        };
  
        fetch('https://ajax.test-danit.com/api/json/posts', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(newPost)
        })
        .then(response => {
            if (response.ok) {
                return response.json();
            }
        })
        .then(data => {
            postModal.style.display = 'none';
            fetch('https://ajax.test-danit.com/api/json/users/1')
              .then(response => {
                  if (response.ok) {
                    return response.json();
              }
              throw new Error('Network response was not ok.');
            }).then((user) => {
              mainContent.insertBefore(new Card(newPost, user).render(), mainContent.firstChild);
            })
        })
    });
  
    fetch('https://ajax.test-danit.com/api/json/users')
    .then(response => {
        if (response.ok) {
            return response.json();
        }
    })
    .then(users => {
        fetch('https://ajax.test-danit.com/api/json/posts')
        .then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
        .then(posts => {
            loadingAnimation.classList.add('hidden');
            mainContent.classList.remove('hidden');
  
            posts.forEach(post => {
                const user = users.find(user => user.id === post.userId);
                const card = new Card(post, user);
                mainContent.appendChild(card.render());
            });
        })
        .catch(error => console.error('Error fetching posts:', error));
    })
    .catch(error => console.error('Error fetching users:', error));
  });
  